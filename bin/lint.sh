#!/usr/bin/env bash

SCRIPT_DIR="$(cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)"
cd "$SCRIPT_DIR/.." || exit

echo -e "\nRunning alex\n"
docker run -v "${PWD}":/workdir -w /workdir \
    registry.gitlab.com/librefoodpantry/common-services/tools/linters/alexjs:latest \
    alex .
