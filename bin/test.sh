#!/usr/bin/env bash

# Set the path to the Dockerfile
DOCKERFILE_PATH="./testing/test-runner/docker-compose.yaml"
DOCKERFILE_PATH2="./testing/test-runner/automated_testing_docker-compose.yaml"

# Temporarily copy the API specefication into the test-runner dir
cp './lib/openapi.yaml' './testing/test-runner/'

# Builds the docker-compose files and runs the tests
docker container prune -f >/dev/null 2>&1
docker-compose -f "$DOCKERFILE_PATH" up -d --remove-orphans >/dev/null 2>&1
docker-compose -f "$DOCKERFILE_PATH2" up --build
docker-compose -f "$DOCKERFILE_PATH" -f "$DOCKERFILE_PATH2" down --remove-orphans >/dev/null 2>&1

rm './testing/test-runner/openapi.yaml'
