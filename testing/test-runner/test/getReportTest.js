const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;

chai.use(chaiHttp);

describe('API tests', function () {
  it('should return a CSV file', function (done) {
    const startDate = '2022-01-01';
    const endDate = '2022-01-31';

    chai.request('http://localhost:10350')
      .get('/WCFB/report?StartDate=2022-09-01&EndDate=2022-09-30')
      .query({ StartDate: startDate, EndDate: endDate })
      .end(function (err, res) {
        expect(err).to.be.null;
        expect(res).to.have.status(400);        
        expect(res).to.have.header("undefined");
        done();
      });
  });
});
