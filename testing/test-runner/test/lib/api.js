const chaiHttp = require('./chaiRequest.js');

module.exports = {
    async listGuests() {
        return chaiHttp.get("/guests");
    },

    async getGuest(id){
        return chaiHttp.get("/guests/" + id)
    },

    async updateGuest(id, data) {
        if(id === undefined){
            id = "1234567";
        }
        if(data === undefined){
            data = {
                "wsuID": id,
                "resident": true,
                "zipCode": "01610",
                "unemployment": false,
                "assistance": {
                    "socSec": false,
                    "TANF": false,
                    "finAid": false,
                    "other": false,
                    "SNAP": false,
                    "WIC": false,
                    "breakfast": false,
                    "lunch": false,
                    "SFSP": false
                },
                "household": [
                    "1",
                    "4"
                ]
            }
        }
        return chaiHttp.put("/guest/" + id).send(data);
    }
};
