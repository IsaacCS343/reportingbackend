const chai = require('chai');
const chaiHttp = require('chai-http');
const { SUT_BASE_URL } = require('./config.js');

const apiContractValidator = require("api-contract-validator");
const apiContractValidatorChaiPlugin = apiContractValidator.chaiPlugin({
  // The path to the OpenAPI specification. (Created in entrypoint.sh)
  // apiDefinitionsPath: "/tmp/openapi.yaml",
  apiDefinitionsPath: "/usr/src/app/lib/openapi.yaml",
  // Produce coverage reports.
  reportCoverage: true,
});


chai.use(apiContractValidatorChaiPlugin);
chai.use(chaiHttp);
const chaiRequest = chai.request(SUT_BASE_URL);

module.exports = chaiRequest;
