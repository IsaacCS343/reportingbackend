const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
const apiVersion = require('../../../src/lib/config.js').API_VERSION;

chai.use(chaiHttp);

describe('Version API', () => {
  it('should return the correct version', (done) => {
    chai.request('http://localhost:10350')
      .get('/WCFB/report/version')
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);        
        expect(res.text).to.equal(apiVersion);        
        done();
      });
  });
});
