const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;

chai.use(chaiHttp);

describe('Version API', () => {
  it('should return the correct version', (done) => {
    chai.request('http://localhost:36381')
      .get('/version')
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body).to.be.an('object');
        expect(res.body.version).to.equal('0.0.0');
        done();
      });
  });
});
