const config = require("../lib/config");

module.exports = {
    method: 'get',
    path: '/version',
    async handler(request, response) {
        const result = config.VERSION;
        response.status(200).json(result);
    }
};