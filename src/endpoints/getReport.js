const Guest = require("../data/guestInfo");
const report = require('../data/reportJsonForm');
const CSV = require('../data/csv');

module.exports = {
  method: 'get',
  path: '/WCFB/report',
  async handler(request, response) {
    let startDate = request.query.StartDate;
    let endDate = request.query.EndDate;
    const array = await Guest.getGuestRecords(startDate, endDate);
    const halfYearArray = await Guest.getHalfYearArray(startDate);
    const result = await report.makingReport(array, halfYearArray);

    response.status(200).send(await CSV.convertToCSV(result));
   
  }
};