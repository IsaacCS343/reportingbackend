const Database = require("../lib/database");
const { ObjectID } = require("mongodb");



class Inventory{
    
    static async getAll() {
        const inventoryCollection = await getInventoryCollection();
        const result = await inventoryCollection.find().toArray();
        return result;
    }

    static async insertInventory(data){
        const inventoryCollection = await getInventoryCollection();
        const result = await inventoryCollection.insertMany(data);
        return result;
    }


    static async convertToISODate(Object){
        const inventoryCollection = await getInventoryCollection();
        const result = await inventoryCollection.updateOne(
            {_id: ObjectID(Object._id)},
            {$set: {dateTime: new Date(Object.dateTime)}},
            { upsert: false });
        if (result.modifiedCount < 1) {
            return null;
        } else {
            const element = await inventoryCollection.findOne(
                {_id: ObjectID(Object._id) }
            );
            return element;
        }          
    }


    static async getInventoryRecords(startDate, endDate){
        let StartDate = new Date(startDate);
        let EndDate = new Date(endDate);
        const inventoryCollection = await getInventoryCollection();
        const result =  await inventoryCollection.find({"dateTime": { $gte : StartDate, $lte : EndDate}}).toArray();
        return result;
    }
   
    
}

async function getInventoryCollection(){
    const database = await Database.get();
    return database.db("inventory").collection("inventory");
}

module.exports = Inventory;