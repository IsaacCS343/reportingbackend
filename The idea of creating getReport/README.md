# Backend diagram


![backend-diagram](backend-diagram.png)

- After storing all the data from Rabbitmq into two collections guestInfo and Inventory, this backend will use the query parameters of endDate and startDate to filter a corresponding guest array from the guestInfo collection, each element in this array is in json form as shown in the picture.
- The guest array will be converted to a report array, where each element in the array represents a column in the [sample report](https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/reportingsystem/community/-/blob/main/Reports/WCFB_Report.md). Then the groupBy() method imported from the underscore library is used to group the report array by zip code.
- The result is a new array where each element in this new array has a key with a zip code value, and the value of each key is a report array with all elements having the same zip code value.
(Note: The type of each element in this new array can be visualized as a HashMap type, <String, array>, in Java or a dictionary type in Python.)

 - The next step is to convert the report array of each zip code key into one report of each zip code. This is done by summing up all the report elements in the report array of each zip code. The array is then converted to a new array where each element of the new array is a report of each zip code.
 - Finally, the toString() method imported from the objects-to-csv library is used to convert the report from json form to text form.


# Testing

This section demonstrates how to generate expected response data to be used for comparison with actual response data.


## 1. Guest information tables:

The two tables below are created based on the dummy data from [RMQ_FakeData.js](../src/send_Fakedata_RMQ/RMQ_FakeData.js)

- All true/false values from dummy data are converted to 1/0 values in the tables below.

- All values of the "Age" columns in the second table are calculated based on the "household" property (the household array).


![guestData](guestData.png)


## 2. Test cases:

To test the response data of this backend, I have split the response data into two different sections. The first section is to check the "new since columns". The second section is to check the remaining columns. I used the equivalence class testing as a tool to come up with some test cases that I could consider to test both sections.

### a. First Section (new since columns):

- The below table is a list of some test cases that should be included to test the new since columns in a report of September.
- Variables in the table: all variables consider the same student.


    **1. Conditions:**
    - first_half_year (Jan to Jun): returns true if the student has at least one visit in this period.
    - second_half_year (Jul to $lt StartDate): returns true if the student has at least one visit in this period.
    - monthly_visit (September): returns true if the student has at least 1 visit in this month.

    **2. Actions:** (X means the action is taken)
    - A new guest: exepting the number of visits in September, the student did not have any other visits since July.
    - Not a new guest: besides the number of visits in September, the student had at least 1 visit in the second_half_year.
    - Ignored: the student does not have any visits in September.

![testCases1](testcases1.png)



### b. Second Section (remaining columns):

- The table below is a list of two cases that should be included to test the remaining columns in a monthly report, specifically September.
- Variables in the table: all variables consider the same student.


    **1. Conditions:** 
    - N: the number of visits by a student in a month.
    - N >= 1: returns true if the student has at least 1 visit in a month. 

    **2. Actions:** (X means the action is taken)
    - Total visits = N: the value of "Total visits" column should be equal to the number of visits by a student in a month.
    - The remaining columns: the value of each column should be calculated base on 1 visit instead of all visits in that month.
    - Ignored: the student has no visits in that month.


![testCases2](testcases2.png)





## 3. Report tables:

- After some test cases were defined, I implemented them by creating a [dummy dataset](../src/send_Fakedata_RMQ/RMQ_FakeData.js) corresponding to conditions of each test case, then I defined the action to be taken in each test case to give the expected result.
- For example, to implement case 1 of the first section of the testing (new since collumns), I have [a dummy data](report1.png) of a student_id with a value of "...12". This student satisfies two conditions, second_half_year and monthly visit (September), of case 1. So, the action should be taken is "Not a new guest". That means the expected value of the "New household" column is 0.
- Based on my dummy dataset and the two tables of test cases, I came up with an expected report of September written into 4 tables shown below.


### Report before grouping by zip code:

![report1](report1.png)

![report2](report2.png)

### Report after grouping by zip code:


- The last three rows of the two tables below are the expected outputs that I can use to compare with the actual outputs of the September report.

![report3](report3.png)

![report4](report4.png)

## 4. Summary:

I believe we have fulfilled all the requirements of this backend to generate monthly reports. Therefore, I think the next step should be taken is to work with the testing code to check the accuracy of this backend. Although we already have the testing code, what we have done includes only a handful of test cases that we believe are important and can be completed during the rest of the semester. So, one of my suggestions if you want to develop the testing code is to create other test case tables, they can be similar to what we did but using different month values, ​such as any month in the first half of the year. Then, based on the conditions of those test cases, you can generate a corresponding dummy dataset. Finally, based on the actions should be taken in each test case to determine the expected output of each report.
